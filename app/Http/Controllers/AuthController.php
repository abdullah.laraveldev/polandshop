<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function adminLogin(Request $request) {
        $request->validate([
            'email'=>'required | email',
            'password'=>'required',
        ]);
//        $user = User::where('email',$request['email'])->first();
        $credentials = [
            'email' => $request['email'],
            'password' => $request['password']
        ];
        $auth = Auth::attempt($credentials);
        if($auth) {
            $user = Auth::user();
            if($user->role == "admin"){
                return view('admin.dashboard');
            }
            else {
                Auth::logout();
                return redirect()->back()->with('error', 'You are not admin. Please enter valid credentials.');
            }
        }
        else {
            return redirect()->back()->with('error', 'Email or Password is incorrect.');
        }
    }

    public function userRegister(Request $request) {
        $request->validate([
            'name'=>'required',
            'username'=>'required|unique:users',
            'email'=>'required|email|unique:users',
            'password'=>'required | min:5',
        ]);
        $user = new User();
        $user->name = $request['name'];
        $user->username = $request['username'];
        $user->email = $request['email'];
        $user->password = Hash::make($request['password']);
        $user->save();
        return redirect()->back()->with('success', 'Register Successfully. Now You Can Login.');
    }

    public function userLogin(Request $request) {
        $request->validate([
            'login_email'=>'required | email',
            'login_password'=>'required | min:5',
        ]);
//        $user = User::where('email',$request['email'])->first();
        $credentials = [
            'email' => $request['login_email'],
            'password' => $request['login_password']
        ];
        $auth = Auth::attempt($credentials);
        if($auth) {
            $user = Auth::user();
            if($user->role == "user"){
                $items = Item::where('status',1)->orderBy('created_at', 'desc')->get();
                return view('front.index', compact('items'));
            }
            else {
                Auth::logout();
                return redirect()->back()->with('error', 'You are not admin. Please enter valid credentials.');
            }
        }
        else {
            return redirect()->back()->with('error', 'Email or Password is incorrect.');
        }
    }

    public function forgotPassword(Request $request) {
        $request->validate([
            'email'=>'required | email',
        ]);
        $user = User::where('email', $request['email'])->first();
        if($user) {
            $password = self::passString(8);
            $user->password = Hash::make($password);
            $data = [
                'to' => $user->email,
                'subject' => "Recover Password",
                'body' => "Congrats! You received new password: ".$password,
            ];
            Mail::send([], [], function ($message) use ($data) {
                $message->to($data['to'])
                    ->subject($data['subject'])
                    ->setBody($data['body'], 'text/html');
            });
            return redirect()->back()->with('success', 'Check your email you have received new password.');
        }
        else {
            return redirect()->back()->with('error', 'This email is not in our record. Try Again!');
        }

    }

    public function passString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getSingleItem($slug) {
        $item = Item::where('slug', $slug)->first();
        if($item) {
            return view('front.single', compact('item'));
        }
        else {
            return redirect()->back()->with('error', 'Item not find.');
        }
    }


}

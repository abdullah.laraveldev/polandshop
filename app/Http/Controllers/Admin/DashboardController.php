<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Recipe;
use App\Models\SubCategory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function dashboard() {
        return view('admin.dashboard');
    }

    public function profileIndex() {
        $authUser = Auth::user();
        return view('admin.profile', compact('authUser'));
    }

    public function updateProfile(Request $request) {
        $auth = Auth::user();
        $request->validate([
            'name'=>'required | min:5',
            'email'=>'required | email',
        ]);

        if($request['password']) {
            $request->validate([
                'password' => 'required | min:5'
            ]);
            $auth->password = Hash::make($request['password']);
        }
        $auth->name = $request['name'];
        $auth->email = $request['email'];
        $auth->save();
        return redirect()->back()->with('success','Profile updated successfully.');
    }

}

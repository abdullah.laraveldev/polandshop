<div class="sidebar-widget brands">
    <h2 class="title">Our Categories</h2>
    @php
        $categories = \App\Models\Category::where('status', 1)->get();
    @endphp
    <ul>
        @if($categories->count() > 0)
            @foreach($categories as $category)
                <li><a href="{{route('front.category.products', $category->slug)}}">{{$category->name}} </a><span>({{$category->getProducts->count()}})</span></li>
            @endforeach
        @else
            No category find
        @endif
    </ul>
</div>

<div class="sidebar-widget widget-slider">
    <div class="sidebar-slider normal-slider">
        @php
            $items = \App\Models\Item::where('status', 1)->orderBy('created_at', 'desc')->take(10)->get();
        @endphp
        @if($items->count() > 0)
            @foreach($items as $item)
                <div class="product-item">
                    <div class="product-title">
                        <a href="{{route('front.product.detail', $item->slug)}}">{{$item->name}}</a>
                        <div class="ratting">
                            <div class="star-rating" title="Rated 4 out of 5"><span
                                    style="width:0%"><strong
                                        class="rating">4</strong> out of 5</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-image">
                        <a href="{{route('front.product.detail', $item->slug)}}">
                            <img src="{{asset('storage/app/public/product/thumbnail/' . $item->thumbnail)}}" alt="Product Image">
                        </a>
                        <div class="product-action">
                            <a href="#"><i class="fa fa-cart-plus"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-search"></i></a>
                        </div>
                    </div>
                    <div class="product-price">
                        <h3><span>$</span>{{$item->display_price}}</h3>
                        <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>
                    </div>
                </div>
            @endforeach
        @else
            No Items find
        @endif


    </div>
</div>

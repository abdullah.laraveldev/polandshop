@extends('front.layouts.main')
@section('title', 'Product Detail')
@section('product_detail', 'active')

@push('front.extra-css')

@endpush

@section('front-content')

    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Products</a></li>
                <li class="breadcrumb-item active">Product Detail</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Product Detail Start -->
    <div class="product-detail">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div class="product-detail-top">
                        <div class="row align-items-center">
                            <div class="col-md-5">
                                @php
                                    $images = json_decode($item->images);
                                @endphp
                                <div class="product-slider-single normal-slider">
                                    @foreach($images as $img)
                                        <img src="{{asset('storage/app/public/product/images/' . $img)}}" alt="Product Image">
                                    @endforeach
                                </div>
                                <div class="product-slider-single-nav normal-slider">
                                    @foreach($images as $img)
                                        <div class="slider-nav-img">
                                            <img src="{{asset('storage/app/public/product/images/' . $img)}}" alt="Product Image">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="product-content">
                                    <div class="title"><h2>{{$item->name}}</h2></div>
                                    <div class="ratting">
                                        <div class="star-rating" title="Rated 4 out of 5"><span
                                                style="width:0%"><strong
                                                    class="rating">4</strong> out of 5</span>
                                        </div>
                                        0.0
                                    </div>
                                    <div class="quantity">
                                        <h4>Category:</h4>
                                        <span>{{isset($item->getCategory)?$item->getCategory->name:"not find"}}</span>
                                    </div>
                                    <div class="price">
                                        <h4>Price:</h4>
                                        <p>${{$item->display_price}} <span>${{$item->original_price}}</span></p>
                                    </div>

                                    <div class="quantity">
                                        <h4>Quantity:</h4>
                                        <div class="qty">
                                            <button class="btn-minus"><i class="fa fa-minus"></i></button>
                                            <input type="text" value="1">
                                            <button class="btn-plus"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div class="action">
                                        <a class="btn" href="#"><i class="fa fa-shopping-cart"></i>Add to Cart</a>
                                        <a class="btn" href="#"><i class="fa fa-shopping-bag"></i>Buy Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row product-detail-bottom">
                        <div class="col-lg-12">
                            <ul class="nav nav-pills nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="#description">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#specification">Specification</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#reviews">Reviews (1)</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div id="description" class="container tab-pane active">
                                    <h4>Product description</h4>
                                    <p>
                                        {!! $item->desc !!}
                                    </p>
                                </div>
                                <div id="specification" class="container tab-pane fade">
                                    <h4>Product specification</h4>
                                    {!! $item->spec !!}
                                </div>
                                <div id="reviews" class="container tab-pane fade">
                                    <div class="reviews-submitted">
                                        <div class="reviewer">Phasellus Gravida - <span>01 Jan 2020</span></div>
                                        <div class="ratting">
                                            <div class="star-rating" title="Rated 4 out of 5"><span
                                                    style="width:90%"><strong
                                                        class="rating">4</strong> out of 5</span>
                                            </div>
                                        </div>
                                        <p>
                                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            doloremque laudantium, totam rem aperiam.
                                        </p>
                                    </div>
                                    <div class="reviews-submit">
                                        <h4>Give your Review:</h4>
                                        <div class="ratting">
                                            <div class="star-rating" title="Rated 4 out of 5"><span
                                                    style="width:90%"><strong
                                                        class="rating">4</strong> out of 5</span>
                                            </div>
                                        </div>
                                        <div class="row form">
                                            <div class="col-sm-6">
                                                <input type="text" placeholder="Name">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="email" placeholder="Email">
                                            </div>
                                            <div class="col-sm-12">
                                                <textarea placeholder="Review"></textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <button>Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--Start Related Products--}}
                    <div class="product">
                        @include('front.partial.related-products')
                    </div>
                    {{--End Related Products--}}

                </div>

                <!-- Side Bar Start -->
                <div class="col-lg-4 sidebar">
                    @include('front.partial.side-widget')
                </div>
                <!-- Side Bar End -->
            </div>
        </div>
    </div>
    <!-- Product Detail End -->

    <!-- Brand Start -->
    <div class="brand">
        @include('front.partial.logo-slider')
    </div>
    <!-- Brand End -->


@endsection

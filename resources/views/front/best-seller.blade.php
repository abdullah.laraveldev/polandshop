@extends('front.layouts.main')
@section('title', 'Best Seller')
@section('best_seller', 'active')

@section('front-content')

    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Best Seller</a></li>
                <li class="breadcrumb-item active">Product List</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Product List Start -->
    <div class="product-view">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-view-top">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="product-search">
                                            <input type="email" value="Search">
                                            <button><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="product-short">
                                            <div class="dropdown">
                                                <div class="dropdown-toggle" data-toggle="dropdown">Product short by</div>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item">Newest</a>
                                                    <a href="#" class="dropdown-item">Popular</a>
                                                    <a href="#" class="dropdown-item">Most sale</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="product-price-range">
                                            <div class="dropdown">
                                                <div class="dropdown-toggle" data-toggle="dropdown">Product price range</div>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item">$0 to $50</a>
                                                    <a href="#" class="dropdown-item">$51 to $100</a>
                                                    <a href="#" class="dropdown-item">$101 to $150</a>
                                                    <a href="#" class="dropdown-item">$151 to $200</a>
                                                    <a href="#" class="dropdown-item">$201 to $250</a>
                                                    <a href="#" class="dropdown-item">$251 to $300</a>
                                                    <a href="#" class="dropdown-item">$301 to $350</a>
                                                    <a href="#" class="dropdown-item">$351 to $400</a>
                                                    <a href="#" class="dropdown-item">$401 to $450</a>
                                                    <a href="#" class="dropdown-item">$451 to $500</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="product-item">
                                <div class="product-title">
                                    <a href="{{route('front.product.detail')}}">Product Name</a>
                                    <div class="ratting">
                                        <div class="star-rating" title="Rated 4 out of 5"><span
                                                style="width:90%"><strong
                                                    class="rating">4</strong> out of 5</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-image">
                                    <a href="product-detail.blade.php">
                                        <img src="{{asset('front-assets/img/product-1.jpg')}}" alt="Product Image">
                                    </a>
                                    <div class="product-action">
                                        <a href="#"><i class="fa fa-cart-plus"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <h3><span>$</span>99</h3>
                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>
                                </div>
                            </div>
                        </div>
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <div class="product-item">--}}
                        {{--                                <div class="product-title">--}}
                        {{--                                    <a href="{{route('front.product.detail')}}">Product Name</a>--}}
                        {{--                                    <div class="ratting">--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-image">--}}
                        {{--                                    <a href="product-detail.blade.php">--}}
                        {{--                                        <img src="{{asset('front-assets/img/product-2.jpg')}}" alt="Product Image">--}}
                        {{--                                    </a>--}}
                        {{--                                    <div class="product-action">--}}
                        {{--                                        <a href="#"><i class="fa fa-cart-plus"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-heart"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-search"></i></a>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-price">--}}
                        {{--                                    <h3><span>$</span>99</h3>--}}
                        {{--                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <div class="product-item">--}}
                        {{--                                <div class="product-title">--}}
                        {{--                                    <a href="{{route('front.product.detail')}}">Product Name</a>--}}
                        {{--                                    <div class="ratting">--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-image">--}}
                        {{--                                    <a href="product-detail.blade.php">--}}
                        {{--                                        <img src="{{asset('front-assets/img/product-3.jpg')}}" alt="Product Image">--}}
                        {{--                                    </a>--}}
                        {{--                                    <div class="product-action">--}}
                        {{--                                        <a href="#"><i class="fa fa-cart-plus"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-heart"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-search"></i></a>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-price">--}}
                        {{--                                    <h3><span>$</span>99</h3>--}}
                        {{--                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <div class="product-item">--}}
                        {{--                                <div class="product-title">--}}
                        {{--                                    <a href="{{route('front.product.detail')}}">Product Name</a>--}}
                        {{--                                    <div class="ratting">--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-image">--}}
                        {{--                                    <a href="product-detail.blade.php">--}}
                        {{--                                        <img src="{{asset('front-assets/img/product-4.jpg')}}" alt="Product Image">--}}
                        {{--                                    </a>--}}
                        {{--                                    <div class="product-action">--}}
                        {{--                                        <a href="#"><i class="fa fa-cart-plus"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-heart"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-search"></i></a>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-price">--}}
                        {{--                                    <h3><span>$</span>99</h3>--}}
                        {{--                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <div class="product-item">--}}
                        {{--                                <div class="product-title">--}}
                        {{--                                    <a href="{{route('front.product.detail')}}">Product Name</a>--}}
                        {{--                                    <div class="ratting">--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-image">--}}
                        {{--                                    <a href="product-detail.blade.php">--}}
                        {{--                                        <img src="{{asset('front-assets/img/product-5.jpg')}}" alt="Product Image">--}}
                        {{--                                    </a>--}}
                        {{--                                    <div class="product-action">--}}
                        {{--                                        <a href="#"><i class="fa fa-cart-plus"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-heart"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-search"></i></a>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-price">--}}
                        {{--                                    <h3><span>$</span>99</h3>--}}
                        {{--                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <div class="product-item">--}}
                        {{--                                <div class="product-title">--}}
                        {{--                                    <a href="{{route('front.product.detail')}}">Product Name</a>--}}
                        {{--                                    <div class="ratting">--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-image">--}}
                        {{--                                    <a href="product-detail.blade.php">--}}
                        {{--                                        <img src="{{asset('front-assets/img/product-6.jpg')}}" alt="Product Image">--}}
                        {{--                                    </a>--}}
                        {{--                                    <div class="product-action">--}}
                        {{--                                        <a href="#"><i class="fa fa-cart-plus"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-heart"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-search"></i></a>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-price">--}}
                        {{--                                    <h3><span>$</span>99</h3>--}}
                        {{--                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <div class="product-item">--}}
                        {{--                                <div class="product-title">--}}
                        {{--                                    <a href="{{route('front.product.detail')}}">Product Name</a>--}}
                        {{--                                    <div class="ratting">--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-image">--}}
                        {{--                                    <a href="product-detail.blade.php">--}}
                        {{--                                        <img src="{{asset('front-assets/img/product-7.jpg')}}" alt="Product Image">--}}
                        {{--                                    </a>--}}
                        {{--                                    <div class="product-action">--}}
                        {{--                                        <a href="#"><i class="fa fa-cart-plus"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-heart"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-search"></i></a>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-price">--}}
                        {{--                                    <h3><span>$</span>99</h3>--}}
                        {{--                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <div class="product-item">--}}
                        {{--                                <div class="product-title">--}}
                        {{--                                    <a href="{{route('front.product.detail')}}">Product Name</a>--}}
                        {{--                                    <div class="ratting">--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-image">--}}
                        {{--                                    <a href="product-detail.blade.php">--}}
                        {{--                                        <img src="{{asset('front-assets/img/product-8.jpg')}}" alt="Product Image">--}}
                        {{--                                    </a>--}}
                        {{--                                    <div class="product-action">--}}
                        {{--                                        <a href="#"><i class="fa fa-cart-plus"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-heart"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-search"></i></a>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-price">--}}
                        {{--                                    <h3><span>$</span>99</h3>--}}
                        {{--                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <div class="product-item">--}}
                        {{--                                <div class="product-title">--}}
                        {{--                                    <a href="{{route('front.product.detail')}}">Product Name</a>--}}
                        {{--                                    <div class="ratting">--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                        <i class="fa fa-star"></i>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-image">--}}
                        {{--                                    <a href="product-detail.blade.php">--}}
                        {{--                                        <img src="{{asset('front-assets/img/product-9.jpg')}}" alt="Product Image">--}}
                        {{--                                    </a>--}}
                        {{--                                    <div class="product-action">--}}
                        {{--                                        <a href="#"><i class="fa fa-cart-plus"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-heart"></i></a>--}}
                        {{--                                        <a href="#"><i class="fa fa-search"></i></a>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="product-price">--}}
                        {{--                                    <h3><span>$</span>99</h3>--}}
                        {{--                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>

                    <!-- Pagination Start -->
                    <div class="col-md-12">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- Pagination Start -->
                </div>

                <!-- Side Bar Start -->
                <div class="col-lg-4 sidebar">
                    <div class="sidebar-widget brands">
                        <h2 class="title">Our Categories</h2>
                        <ul>
                            <li><a href="#">Snacks </a><span>(45)</span></li>
                            <li><a href="#">Candy </a><span>(34)</span></li>
                            <li><a href="#">Grocery </a><span>(67)</span></li>
                            <li><a href="#">General Merchandise </a><span>(74)</span></li>
                            <li><a href="#">Phone Accessories </a><span>(89)</span></li>
                            <li><a href="#">Cigars </a><span>(28)</span></li>
                            <li><a href="#">Vape </a><span>(32)</span></li>
                            <li><a href="#">Tobacco </a><span>(25)</span></li>
                            <li><a href="#">Cigarettes </a><span>(102)</span></li>
                            <li><a href="#">Beer </a><span>(30)</span></li>
                            <li><a href="#">Wine </a><span>(34)</span></li>
                            <li><a href="#">Liquor </a><span>(35)</span></li>
                        </ul>
                    </div>

                    <div class="sidebar-widget widget-slider">
                        <div class="sidebar-slider normal-slider">
                            <div class="product-item">
                                <div class="product-title">
                                    <a href="#">Product Name</a>
                                    <div class="ratting">
                                        <div class="star-rating" title="Rated 4 out of 5"><span
                                                style="width:90%"><strong
                                                    class="rating">4</strong> out of 5</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-image">
                                    <a href="product-detail.blade.php">
                                        <img src="{{asset('front-assets/img/product-7.jpg')}}" alt="Product Image">
                                    </a>
                                    <div class="product-action">
                                        <a href="#"><i class="fa fa-cart-plus"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <h3><span>$</span>99</h3>
                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>
                                </div>
                            </div>
                            <div class="product-item">
                                <div class="product-title">
                                    <a href="#">Product Name</a>
                                    <div class="ratting">
                                        <div class="star-rating" title="Rated 4 out of 5"><span
                                                style="width:90%"><strong
                                                    class="rating">4</strong> out of 5</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-image">
                                    <a href="product-detail.blade.php">
                                        <img src="{{asset('front-assets/img/product-8.jpg')}}" alt="Product Image">
                                    </a>
                                    <div class="product-action">
                                        <a href="#"><i class="fa fa-cart-plus"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <h3><span>$</span>99</h3>
                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>
                                </div>
                            </div>
                            <div class="product-item">
                                <div class="product-title">
                                    <a href="#">Product Name</a>
                                    <div class="ratting">
                                        <div class="star-rating" title="Rated 4 out of 5"><span
                                                style="width:90%"><strong
                                                    class="rating">4</strong> out of 5</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-image">
                                    <a href="product-detail.blade.php">
                                        <img src="{{asset('front-assets/img/product-9.jpg')}}" alt="Product Image">
                                    </a>
                                    <div class="product-action">
                                        <a href="#"><i class="fa fa-cart-plus"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <h3><span>$</span>99</h3>
                                    <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Side Bar End -->
            </div>
        </div>
    </div>
    <!-- Product List End -->

    <!-- Brand Start -->
    <div class="brand">
        <div class="container-fluid">
            <div class="brand-slider">
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-1.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-2.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-3.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-4.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-5.png')}}" alt=""></div>
                <div class="brand-item"><img src="{{asset('front-assets/img/brand-6.png')}}" alt=""></div>
            </div>
        </div>
    </div>
    <!-- Brand End -->

@endsection


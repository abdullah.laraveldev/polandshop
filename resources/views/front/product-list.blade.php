@extends('front.layouts.main')
@section('title', 'Products')
@section('products', 'active')
@push('front.extra-css')
    <style>
        .custom-search-button {
            background-color: #CF2027;
            border: none;
            padding: 5px 20px 5px 20px;
            color: white;
            border-radius: 5px;
        }
    </style>
@endpush
@section('front-content')
    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Products</a></li>
                <li class="breadcrumb-item active">List</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Product List Start -->
    <div class="product-view">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-view-top">
                                <form action="" method="post">
                                @csrf
                                <div class="row">
                                        <div class="col-md-4">
                                            <div class="product-search">
                                                <input type="text" placeholder="product name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="product-short">
                                                <div class="dropdown">
                                                    <select name="sort_by" id="" class="form-control">
                                                        <option selected disabled>Product short by</option>
                                                        <option value="Newest">Newest</option>
                                                        <option value="Popular">Popular</option>
                                                        <option value="Most sale">Most sale</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="product-price-range">
                                                <select name="price_range" id="" class="form-control">
                                                    <option selected disabled>Product price range</option>
                                                    <option value="$0 to $50">$0 to $50</option>
                                                    <option value="$51 to $100">$51 to $100</option>
                                                    <option value="$101 to $150">$101 to $150</option>
                                                    <option value="$151 to $200">$151 to $200</option>
                                                    <option value="$201 to $250">$201 to $250</option>
                                                    <option value="$251 to $300">$251 to $300</option>
                                                    <option value="$301 to $350">$301 to $350</option>
                                                    <option value="$351 to $400">$351 to $400</option>
                                                    <option value="$401 to $450">$401 to $450</option>
                                                    <option value="$451 to $500">$451 to $500</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="custom-search-button"><i class="fa fa-search"></i></button>
                                        </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        @if($items->count() > 0)
                        @foreach($items as $item)
                            <div class="col-md-4">
                                <div class="product-item">
                                    <div class="product-title">
                                        <a href="{{route('front.product.detail', $item->slug)}}">{{$item->name}}</a>
                                        <div class="ratting">
                                            <div class="star-rating" title="Rated 4 out of 5"><span
                                                    style="width:0%"><strong class="rating">4</strong> out of 5</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-image">
                                        <a href="product-detail.blade.php">
                                            <img src="{{asset('storage/app/public/product/thumbnail/' . $item->thumbnail)}}" alt="Product Image">
                                        </a>
                                        <div class="product-action">
                                            <a href="#"><i class="fa fa-cart-plus"></i></a>
                                            <a href="#"><i class="fa fa-heart"></i></a>
                                            <a href="#"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="product-price">
                                        <h3><span>$</span>{{$item->display_price}}</h3>
                                        <a class="btn" href=""><i class="fa fa-shopping-cart"></i>Buy Now</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @else
                            No Items Available.
                        @endif
                    </div>

                    <!-- Pagination Start -->
                    <div class="col-md-12">
                        <nav aria-label="Page navigation example">
                            {{$items->links('front.partial.paginate')}}
                        </nav>
                    </div>
                    <!-- Pagination Start -->
                </div>

                <!-- Side Bar Start -->
                <div class="col-lg-4 sidebar">
                    @include('front.partial.side-widget')
                </div>
                <!-- Side Bar End -->
            </div>
        </div>
    </div>
    <!-- Product List End -->

    <!-- Brand Start -->
    <div class="brand">
        @include('front.partial.logo-slider')
    </div>
    <!-- Brand End -->

@endsection

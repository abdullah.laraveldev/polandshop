@extends('admin.layouts.layout')
@push('dashboard.scripts-head')
@endpush
@section('dashboard.content-view')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-content collapse show">
                    <div class="card-footer text-center p-1">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-red border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Total Revenue</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-fast-forward mr-1"></i>
                                                {{isset($subCat)?$subCat:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-grey-blue border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Last Month Revenue</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-wind mr-1"></i>
                                                {{isset($recipe)?$recipe:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-grey-blue border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Last Week Revenue</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-wind mr-1"></i>
                                                {{isset($recipe)?$recipe:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-info border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Users</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-users mr-1"></i>
                                                {{isset($users)?$users:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-primary border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Categories</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-grid mr-1"></i>
                                                {{isset($subAdmins)?$subAdmins:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-golden border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Products</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-grid mr-1"></i>
                                                {{isset($category)?$category:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-red border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Total Orders</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-fast-forward mr-1"></i>
                                                {{isset($subCat)?$subCat:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-grey-blue border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Pending Order</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-wind mr-1"></i>
                                                {{isset($recipe)?$recipe:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-md-4 col-sm-12">
                                <div class="card pull-up border-top-grey-blue border-top-3 rounded-0">
                                    <div class="card-header">
                                        <h4 class="card-title" style="font-weight: 500;">Delivered Order</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body p-1">
                                            <h4 class="font-large-1 text-bold-400">
                                                <i class="ft-wind mr-1"></i>
                                                {{isset($recipe)?$recipe:0}}
                                            </h4>
                                        </div>
                                        <div class="card-footer p-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard.scripts-footer')
@endpush
